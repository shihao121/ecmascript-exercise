// TODO 14: 在这里写实现代码
import Person from './person';
// eslint-disable-next-line no-unused-vars

export default class Worker extends Person {
  // eslint-disable-next-line no-useless-constructor
  constructor(name, age) {
    super(name, age);
  }

  introduce() {
    return `${super.introduce()} I am a Worker. I have a job.`;
  }
}
