// TODO 11: 在这里写实现代码
export default // eslint-disable-next-line no-unused-vars
class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  introduce() {
    return `My name is ${this.name}. I am ${this.age} years old.`;
  }
}
